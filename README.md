```java
public class Me extends Person {
    private final String firstName = "Mykhailo";
    private final String lastName = "Musiienko";
    public String email = "m_musienko@outlook.com";
    public String status = "I’m currently learning Java";
    public String[] skills = {"Java", "JavaScript", "HTML", "CSS"};
    public void sayHello() {
        System.out.println("Hello there!");
    }
}
```

<div align="center">
 <img src="https://gitlab.com/m_musiienko/images/-/raw/master/img/code.gif" />
</div>

<br>

<div align="center">
 <h3>Let's connect and chat.</h3>
</div>

<div align="center">
 <a href="https://twitter.com/Michael22878035"><img src="https://gitlab.com/m_musiienko/images/-/raw/master/icons/twitter-fill.png" /></a>
 <a href="mailto:m_musienko@outlook.com"><img src="https://gitlab.com/m_musiienko/images/-/raw/master/icons/mail-fill.png" /></a>
 <a href="https://www.linkedin.com/in/mykhailo-musiienko-80849880/"><img src="https://gitlab.com/m_musiienko/images/-/raw/master/icons/linkedin-box-fill.png" /></a>
 <a href="https://t.me/Mykhailo_Musiienko"><img src="https://gitlab.com/m_musiienko/images/-/raw/master/icons/telegram-fill.png" /></a>
 <a href="https://github.com/mypage-solutions"><img src="https://gitlab.com/m_musiienko/images/-/raw/master/icons/github-fill.png" /></a>
</div>
